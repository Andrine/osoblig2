#!/bin/bash

pid=$(pgrep -f firefox);
for pid in $pid;
do
pageFault=$(ps --no-headers -o maj_flt "$pid");
if [ "$pageFault" -gt 1000 ]
then
echo "Firefox $pid har forårsaket $pageFault major page faults (har over 1000)"
else
echo "Firefox $pid har forårsaket $pageFault major page faults"
fi
done

