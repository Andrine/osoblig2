#!/bin/bash
function menu {
echo
echo "1-Hvem er jeg og hva er navnet på dette scriptet?"
echo "2 - Hvor lenge er det siden siste boot?"
echo "3 - Hvor mange prosesser og traader finnes?"
echo "4 - Hvor mange context switch er fant sted siste sekund?"
echo "5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
echo "6 - Hvor mange interrupts fant sted siste sekund?"
echo "9-Avslutt dette scriptet"
echo
}
menu
while read valg
do
PS3='Velg en funksjon '
	case $valg in
		1)
			echo "User: $(whoami)"
			echo "Script name:$(basename -- "$0")"
			;;
		2)
			uptime -p
			;;
		3)	echo "Prosesser: $(ps aux --no-heading | wc -l)"
			echo "Thread: $(ps -eLF --no-heading | wc -l)"
			
			;;
		4)
			C1=$(grep ctxt /proc/stat | awk '{print $2}');
			sleep 1;
			C2=$(grep ctxt /proc/stat | awk '{print $2}');
			 let "Csum=$C2-$C1";
			echo "Context i sek: $Csum"
			;;
		5)
			Kmode1=$(grep -w cpu /proc/stat | awk '{print $4}');
			Umode1=$(grep -w cpu /proc/stat | awk '{print $2}');
			sleep 1;
			Kmode2=$(grep -w cpu /proc/stat | awk '{print $4}');
			Umode2=$(grep -w cpu /proc/stat | awk '{print $2}');
			let "KmodeSum=$Kmode2-$Kmode1";
			let "UmodeSum=$Umode2-$Umode1";
			echo "Kernel mode i sek: $KmodeSum"
			echo "User mode i sek: $UmodeSum"
			;;
		6)
			intr1=$(grep -w intr /proc/stat | awk '{print $2}');
			sleep 1;
			intr2=$(grep -w intr /proc/stat | awk '{print $2}');
			let "intrSum=$intr2-$intr1";
			echo "interrupts i sek: $intrSum"
			;;
		9)	
			exit;;

		*)
	esac
menu
done
