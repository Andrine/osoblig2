#!/bin/bash

function fil(){
{
echo "******** Minne info om prosess med PID $pid ********"

echo "Total bruk av virtuelt minne: $VmSize kB"

echo "Mengde privat virtuelt minne: $VmPri kB" 

echo "Mengde shared virtuelt minne: $VmLib kB"

echo "Totalt bruk ac fysisk minne: $VmRSS kB"

echo "Mangde fysisk minne som benyttes til page table: $VmPTE kB"
} > "$filNavn";

}

dato=$(date +"%Y%m%d");
for pid in "$@";
do
if [[ -r /proc/$pid/status && -e /proc/$pid/status ]]; then
filNavn="$pid-$dato.meminfo";
VmSize=$(grep VmSize /proc/"$pid"/status | awk '{print $2}');
VmData=$(grep VmData /proc/"$pid"/status | awk '{print $2}');
VmStk=$(grep VmStk /proc/"$pid"/status | awk '{print $2}');
VmExe=$(grep VmExe /proc/"$pid"/status | awk '{print $2}');
VmLib=$(grep VmLib /proc/"$pid"/status | awk '{print $2}');
VmRSS=$(grep VmRSS /proc/"$pid"/status | awk '{print $2}');
VmPTE=$(grep VmPTE /proc/"$pid"/status | awk '{print $2}');
if [[ $VmData -gt 0 && $VmStk -gt 0 && $VmExe -gt 0 ]]; then
let "VmPri=$VmData+$VmStk+$VmExe";
fil "$pid $VmSize $VmPri $VmLib $VmRSS $VmPTE $filNavn"
else
 echo "Fant ikke mengden av privat virtuelt minne"
fi
else
	echo "Pid er ikke gydlig!"
fi
done



